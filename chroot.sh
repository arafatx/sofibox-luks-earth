#!/bin/sh

# Author: Arafat Ali (arafat@sofibox.com)

password="password-here"

echo "list partition: "    
lsblk -f #list partition

#sudo cryptsetup luksOpen /dev/sda2 box_earth
echo "check if encrypted partition is opened"
if [ ! -b /dev/mapper/box_earth ]; then
      echo "encrypted partition is not opened ..."
      echo "opening luks partition ... [input password]"
      sudo echo -n "$password" | cryptsetup luksOpen /dev/sda2 box_earth -
fi

echo "===================================="
echo "mounting luks lvm partition (root) ..."
sudo mount /dev/mapper/box_earth-root /mnt
echo "mounting luks lvm partition (dev) ..."
sudo mount --bind /dev /mnt/dev
echo "mounting luks lvm partition (proc) ..."
sudo mount --bind /proc /mnt/proc
echo "mounting luks lvm partition (sys) ..."
sudo mount --bind /sys /mnt/sys


# -p if folder not exist
echo "creating /mnt/boot/efi dir if not exist ..."
sudo mkdir -p /mnt/boot/efi || true
echo "mounting boot partition ..."
sudo mount /dev/sda1 /mnt/boot/efi

echo "running chroot on /mnt: PRESS CTRL + D to exit"
# run chroot    
sudo chroot /mnt