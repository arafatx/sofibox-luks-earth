#!/bin/sh

#This script will be triggered from csf load warning. So always reported warning

REPORT_FILE="/usr/local/bin/maxicron/csf/log/csf.report"
MAIL_BIN="/usr/local/bin/mail"
MYEMAIL="webmaster@sofibox.com"
MYHOSTNAME=`/bin/hostname`

echo "[csf | info]: CSF Server Load Checked on `date`" | tee -a $REPORT_FILE
echo "---------------------------------------------------" | tee -a $REPORT_FILE
cat /dev/null > $REPORT_FILE
iostat -d 1 5 | tee -a $REPORT_FILE
echo "---" | tee -a $REPORT_FILE
iostat -x -d 1 5 | tee -a $REPORT_FILE
echo "---" | tee -a $REPORT_FILE
mpstat -P ALL | tee -a $REPORT_FILE
echo "---" | tee -a $REPORT_FILE
top -b -n 1 | tee -a $REPORT_FILE
echo "---" | tee -a $REPORT_FILE
netstat -autpn | tee -a $REPORT_FILE
echo "---" | tee -a $REPORT_FILE
echo "---------------------------------------------------" | tee -a $REPORT_FILE

echo "[csf | info]: Status: WARNING" | tee -a $REPORT_FILE
echo "===================================================" | tee -a $REPORT_FILE
$MAIL_BIN -s "[csf | WARNING]: CSF Reported High Load Stress @ $MYHOSTNAME" $MYEMAIL < $REPORT_FILE