#!/bin/sh

PATH=/bin:/usr/bin:/sbin:/usr/sbin
export PATH

# Locations of binaries
GREP="/bin/grep"
HOST=`hostname`
MAIL="/bin/mail"
MKTEMP="/bin/mktemp"
YUM="/bin/yum"
WARNING_STATUS="N/A"
YUM_LOG_PATH="/usr/local/bin/maxicron/yum/log"
# Who to E-mail with new updates
ADMIN=webmaster@sofibox.com

mkdir -p ${YUM_LOG_PATH}
WORK="$YUM_LOG_PATH/yum.results.log"
REPORT="$YUM_LOG_PATH/yum.report.log"
cat /dev/null > ${REPORT}
cat /dev/null > ${WORK}

echo "=================================================================="
echo "[yum | info]: Yellowdog Updater Modified (YUM) is checking for system update ..."
echo "Yellowdog Updater Modified (YUM) script started on `date`" >> ${REPORT}
# Dump the yum results to a safe working file

${YUM} -e0 -d0 check-update >> ${WORK}

# If there are updates available, E-mail them
if [ -s ${WORK} ];then
 WARNING_STATUS="WARNING"
echo "[yum | ${WARNING_STATUS}]: The following updates are available for ${HOST} [checked on `date`]:" >> ${REPORT}
cat ${WORK} >> ${REPORT}
cat ${REPORT} | ${MAIL} -s "[yum | ${WARNING_STATUS}]: Yellowdog Updater Modified Check Report @ ${HOST}" ${ADMIN}
else
 WARNING_STATUS="OK"
 echo "[yum | ${WARNING_STATUS}]: There were no updates available for ${HOST} [checked  on `date`]" > ${REPORT}
#cat ${REPORT} | ${MAIL} -s "[yum | ${WARNING_STATUS}]: Yellowdog Updater Modified Check Report @ ${HOST}" ${ADMIN}
fi

# Cleanup temporary files
#rm ${REPORT} ${WORK}
echo "[yum | info]: Checking status: ${WARNING_STATUS}"
echo "[yum | info]: Done checking system update for [${HOST}]. Email notification is set to [${ADMIN}]"
echo "=================================================================="