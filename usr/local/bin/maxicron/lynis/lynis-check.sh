#!/bin/sh
#lynis-check.sh - EARTH.SOFIBOX.COM

echo "=============================================================================="
echo "[lynis] Lynis System Audit is checking system..."

#Global Variables
#REPORT_FILE_TEMP="/tmp/lynis-report-temp.log"
REPORT_FILE_GREP="/tmp/lynis-report-grep.log"
REPORT_FILE_FINAL="/tmp/lynis-report-final.log"
MYHOSTNAME=`/bin/hostname`
LYNIS_BIN="/usr/local/lynis/./lynis"
MAIL_BIN="/usr/local/bin/mail"
MYEMAIL="webmaster@sofibox.com"
WARNING_STATUS="N/A"

#cat /dev/null > $REPORT_FILE_TEMP
cat /dev/null > $REPORT_FILE_GREP
cat /dev/null > $REPORT_FILE_FINAL

echo "==============================================================================" > $REPORT_FILE_FINAL
echo "Lynis checked on `date`" >> $REPORT_FILE_FINAL
#echo "Warnings, WARNING, Suggestions and Exceptions found:" >> $REPORT_FILE_GREP
$LYNIS_BIN update info >> $REPORT_FILE_FINAL
$LYNIS_BIN --cronjob >> $REPORT_FILE_FINAL
#echo "Warnings, WARNING, Suggestions, Exception found" >> $REPORT_FILE_TEMP

echo "Lynis security audit keywords: Warnings, WARNING, Suggestions and Exceptions found:" >> $REPORT_FILE_GREP
sed $'s/[^[:print:]\t]//g' $REPORT_FILE_FINAL >> $REPORT_FILE_GREP

if (grep 'Warnings\|WARNING\|Exceptions found\|Suggestions' $REPORT_FILE_GREP >> $REPORT_FILE_FINAL) then
 #cat $REPORT_FILE_SED >> $REPORT_FILE_FINAL
 WARNING_STATUS="WARNING"
 #cat $REPORT_FILE_TEMP
 $MAIL_BIN -s "[lynis][$WARNING_STATUS|N] Lynis System Audit Scan Report @ $MYHOSTNAME" $MYEMAIL < $REPORT_FILE_FINAL
else
 WARNING_STATUS="OK"
 # Do nothing
fi

#rm -rf $REPORT_FILE_TEMP
#rm -rf $REPORT_FILE_SED

echo "[lynis] Scan status: $WARNING_STATUS"
echo "[lynis] Done checking system. Email notification is set to webmaster@sofibox.com"
echo "=================================================================================="