To use maxiclam everywhere in the terminal run this command:

ln -s /usr/local/bin/maxicron/clamav/maxiclam /usr/local/bin

Then we can execute:

maxiclam #This will scan current directory and report infected files with error

maxiclam -i #This will scan current directory and report only infected files without error

maxiclam /tmp #This will scan /tmp directory and report infected files with error

maxiclam /tmp -i or maxiclam -i /tmp #This will scan /tmp directory and report only infected files without error

#for more options, execute maxiclam /? or maxiclam -h

![maxiclam](img/screenshot1.PNG "maxiclam")
