#!/bin/sh

#Global Variables
GIT_LOG_PATH="/usr/local/bin/maxicron/git/log"
REPORT_DATE="$(date '+%d-%m-%Y_%H-%M-%S')" #31-03-2020--11-56-16
REPORT_FILE="/usr/local/bin/maxicron/git/log/git-software.log"
REPORT_FILE_FINAL="/usr/local/bin/maxicron/git/log/git-software-final.log"
REPORT_FILE_FINAL_MULTI="/usr/local/bin/maxicron/git/log/git-software-final-$REPORT_DATE.log"
REPORT_FILE_GIT_TEMP="/usr/local/bin/maxicron/git/log/git-software-temp-status.log"
MAIL_BIN="/usr/local/bin/mail"
MYHOSTNAME=`/bin/hostname`
MYEMAIL="webmaster@sofibox.com"

# Prepare log files
###############################

mkdir -p $GIT_LOG_PATH

# Delete log files that is older than 5 days | create dir first before running this
find $GIT_LOG_PATH -name "*.log" -mtime +5 -exec rm {} \;

touch $REPORT_FILE
chown root:adm $REPORT_FILE
chmod 640 $REPORT_FILE
cat /dev/null > $REPORT_FILE

touch $REPORT_FILE_FINAL
chown root:adm $REPORT_FILE_FINAL
chmod 640 $REPORT_FILE_FINAL
cat /dev/null > $REPORT_FILE_FINAL

touch $REPORT_FILE_FINAL_MULTI
chown root:adm $REPORT_FILE_FINAL_MULTI
chmod 640 $REPORT_FILE_FINAL_MULTI
cat /dev/null > $REPORT_FILE_FINAL_MULTI

touch $REPORT_FILE_GIT_TEMP
chown root:adm $REPORT_FILE_GIT_TEMP
chmod 640 $REPORT_FILE_GIT_TEMP
cat /dev/null > $REPORT_FILE_GIT_TEMP

################################

## START CHECKING RUNNING DUPLICATED PROCESS ##
script_name=$(basename -- "$0")
pid=(`pgrep -f $script_name`)
pid_count=${#pid[@]}

#[[ -z $pid ]] && echo "[git | error]: Failed to get the PID" | tee -a $REPORT_FILE

if [ -f "/var/run/$script_name" ];then
        if [[  $pid_count -gt "1" ]];then
                WARNING_STATUS="WARNING"
                echo "==================================================================================" | tee -a $REPORT_FILE
                echo "[git | info]: Error: an another instance of $script_name script is already running, " | tee -a $REPORT_FILE
                echo "clear all the sessions of $script_name to initialize session" | tee -a $REPORT_FILE
                echo "the $script_name script is terminated for this reason" | tee -a $REPORT_FILE
                echo "==================================================================================" | tee -a $REPORT_FILE
                $MAIL_BIN -s "[git | $WARNING_STATUS]: Git Application Update Scan Report @ $MYHOSTNAME" $MYEMAIL <$REPORT_FILE
                echo "[git | info]: Email notification is set to $MYEMAIL"
                echo "=================================================================================="
                #Remove log and lock status
                #rm -f $REPORT_FILE
                rm -f "/var/run/$script_name"
                # terminate script
                cat $REPORT_FILE >> $REPORT_FILE_FINAL_MULTI
                exit 1
        else
                echo "==========================================================================================================" | tee -a $REPORT_FILE
                echo "[git | info]: Warning: the last instance of $script_name script exited unsuccessfully, performing cleanup..." | tee -a $REPORT_FILE
                rm -f "/var/run/$script_name"
                echo "[git | info]: An instance of $script_name script has been removed successfully from the lock" | tee -a $REPORT_FILE
                echo "==========================================================================================================" | tee -a $REPORT_FILE
        fi
fi
## END CHECKING RUNNING DUPLICATED PROCESS ##
## SAVE PROCESS INFO
echo $pid > /var/run/$script_name
echo "==========================================================================================================" | tee -a $REPORT_FILE
echo "[git | info]: Git software update script started on `date`" >> $REPORT_FILE
echo "[git | info]: Git software update is checking for latest update ..."
echo "[git | info]: Please wait ..."
declare -a git_locs=("/usr/local/bin/maxicron/git/test-update"
                        "/usr/local/lynis/"
                        "/usr/local/directadmin/custombuild/custom/roundcube/plugins/rcguard/"
                        "/usr/local/chkrootkit/")
count=0
avail_update_count=0
echo "---------" | tee -a $REPORT_FILE
for i in "${git_locs[@]}"
do
        #echo "$i"
        cd $i
        remote_url=`git config --get remote.origin.url`
        remote_url_bname=`basename $(git rev-parse --show-toplevel)`
        #bname=`basename $PWD`
        count=`expr $count + 1`
        #echo ""
        echo "[$count] [git | info]: Checking git update for [ $remote_url_bname ] ..." | tee -a $REPORT_FILE
        echo "---------------------------" | tee -a $REPORT_FILE
        echo "Local Path: $PWD" | tee -a $REPORT_FILE
        echo "Remote URL: $remote_url" | tee -a $REPORT_FILE
        git remote update
        cat /dev/null > $REPORT_FILE_GIT_TEMP
        git status | tee -a $REPORT_FILE_GIT_TEMP | tee -a $REPORT_FILE
        # For update
        if (grep -e "Your branch is behind" $REPORT_FILE_GIT_TEMP >> /dev/null) then
                WARNING_STATUS="WARNING"
                #echo "[WARNING] $remote_url_bname at $PWD has something to update from git" | tee -a $REPORT_FILE
                echo "[WARNING] UPDATE FOUND for $remote_url_bname" | tee -a $REPORT_FILE
                if [ "$i" == "/usr/local/directadmin/custombuild/custom/roundcube/plugins/rcguard/" ]; then
                        echo "  (a) Use git pull and (b) then ./build roundcube to replace update files at /var/www/html/roundcube/plugins" >> $REPORT_FILE
                fi
                avail_update_count=`expr $avail_update_count + 1`
        # For changes commit
        elif (grep -E 'Changes to be committed|Changes not staged for commit' $REPORT_FILE_GIT_TEMP >> /dev/null) then
                WARNING_STATUS="WARNING"
                echo "[WARNING] $remote_url_bname has something to commit" | tee -a $REPORT_FILE
                                avail_update_count=`expr $avail_update_count + 1`
        else
                #echo "[OK] no update found for $remote_url_bname at $PWD" | tee -a $REPORT_FILE
                echo "[OK] NO UPDATE FOUND for $remote_url_bname" | tee -a $REPORT_FILE
        fi
        echo "---------------------------" | tee -a $REPORT_FILE
        #echo "[git | info]: Done checking git update for $remote_url_bname at $PWD"
        echo "" | tee -a $REPORT_FILE
done
echo "" | tee -a $REPORT_FILE
echo "-------" >> $REPORT_FILE

### next for rspamd.. directory: /usr/local/directadmin/plugins/rspamd/
echo "[git | info]: Scan status: $WARNING_STATUS" | tee -a $REPORT_FILE
echo "[git | info]: Total updateable git repo: $avail_update_count/$count" | tee -a $REPORT_FILE
echo "[git | info]: Done checking system. Email notification is set to $MYEMAIL"
echo "==========================================================================================================" | tee -a $REPORT_FILE

if [ "$WARNING_STATUS" == "WARNING" ]; then
        sed $'s/[^[:print:]\t]//g' $REPORT_FILE > $REPORT_FILE_FINAL
        $MAIL_BIN -s "[git | $WARNING_STATUS | $avail_update_count update(s)] Git Application Update Scan Report @ $MYHOSTNAME" $MYEMAIL < $REPORT_FILE_FINAL
fi

cat $REPORT_FILE_FINAL >> $REPORT_FILE_FINAL_MULTI
rm -f "/var/run/$script_name"