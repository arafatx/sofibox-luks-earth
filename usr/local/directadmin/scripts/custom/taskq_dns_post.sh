#!/bin/sh
# Author - Arafat Ali
# Remove the debug comment to run in real environment
#DA wildcard request for linode external DNS
# Note: The beta release of DA has a new feature that able to pull ssl certs from external DNS. It supports 70+ provider
# Currently as today 2nd August 2020, the feature is marked as unfinished (testing) https://directadmin.com/features.php?id=2825
# Find more information and discussion here: https://directadmin.com/features.php?id=2825
# If you are using directadmin beta version (with recent build), you can use the built-in feature of DA
# But, if you are in a production server, requesting wild certificate ssl from external DNS currently is not possible. So this script is an example how to overcome the problem:

LPYENV="/home/user/maxipy/bin/./linode-cli"
MYEMAIL="webmaster@sofibox.com"
MYHOSTNAME=`/bin/hostname`
WARNING_STATUS="OK"
MAIL_BIN="/usr/local/bin/mail"
DATE_BIN=`/usr/bin/date +%s`
RM_BIN="/bin/rm"
MV_BIN="/bin/mv"
REPORT_FILE="/tmp/taskq-dns-post-report.log"
#cat /dev/null > $REPORT_FILE
#DEFINED VARIABLES PASSED FOR taskq_dns_post.sh:
DOMAIN=$domain
#DOMAIN="lozira.com" # For Debug
DO_ACTION=$do
#DO_ACTION="add" # Debug
TYPE=$type
#TYPE="TXT" # Debug
NAME=$name
#NAME="_acme-challenge-test" # Debug
VALUE=`echo "$value" | tr -d '"'` #remove the double quotation
#VALUE="pre-check"
TTL=$ttl # default 5
DNS_USER_TTL_SEC="5"
WILDCARD=$wildcard #yes or no
#WILDCARD="yes" # Debug
WILDCARD_SELECT0=$le_wc_select0
WILDCARD_SELECT1=$le_wc_select1
#WILDCARD_SELECT0=*.lozira.com # Debug
#WILDCARD_SELECT1=lozira.com # Debug
LINODE_WILDCARD_SELECT="*"
# Can be obtain from here: /usr/local/directadmin/data/admin/ips
ipv4="youripv4" # add your ipv4 here for the wildcard target ipv4
ipv6="youripv6" # add your ipv6 here for the wildcard target ip6
# Debug data
echo "=====================================" | tee -a $REPORT_FILE
echo "====== RAW DATA [$DO_ACTION] ========" | tee -a $REPORT_FILE
echo "=====================================" | tee -a $REPORT_FILE
env | grep -v pass > $0.env
cat $0.env >> $REPORT_FILE
echo "=====================================" | tee -a $REPORT_FILE

# get Domain ID and print it out (OK)
getLinodeDomainID()
{
    $LPYENV domains list --domain $DOMAIN --pretty --json |
        fgrep '"id":' |
        sed -r 's/[^0-9]//g'
}

# get existing DNS record name for _acme-challenge or _acme-challenge-test and print it out (OK)
getDNSRecordNameACME()
{
    LinodeDomainID=$( getLinodeDomainID $DOMAIN)

    if [ -n "$LinodeDomainID" ]; then
        if [ "$NAME" == "_acme-challenge" ]; then
            $LPYENV domains records-list --page 2 $LinodeDomainID --pretty --json | grep "\"name\": \"$NAME\"" | grep -o '_acme-challenge'
        elif [ "$NAME" == "_acme-challenge-test" ]; then
            $LPYENV domains records-list --page 2 $LinodeDomainID --pretty --json | grep "\"name\": \"$NAME\"" | grep -o '_acme-challenge-test'
        fi
    fi
}


# get existing DNS record name for wildcard domains (eg. with 2 common names:  .*sofibox.com, sofibox.com) and print it out (OK)
getDNSRecordNameExtWildCardA()
{
    LinodeDomainID=$( getLinodeDomainID $DOMAIN)

    if [ -n "$LinodeDomainID" ]; then
        $LPYENV domains records-list --page 1 $LinodeDomainID --pretty --json | jq '.[] | select((.name=="*") and (.type=="A"))'
    fi
}

# get existing DNS record name for wildcard domains (eg. with 2 common names:  .*sofibox.com, sofibox.com) and print it out (OK)
getDNSRecordNameExtWildCardAAAA()
{
    LinodeDomainID=$( getLinodeDomainID $DOMAIN)

    if [ -n "$LinodeDomainID" ]; then
        $LPYENV domains records-list --page 1 $LinodeDomainID --pretty --json | jq '.[] | select((.name=="*") and (.type=="AAAA"))'
    fi
}

#get existing DNS record ID specific to _acme-challenge (OK)
getDNSRecordID()
{
    LinodeDomainID=$(getLinodeDomainID $DOMAIN)

    if [ -n "$LinodeDomainID" ]; then
        $LPYENV domains records-list --page 2 $LinodeDomainID --pretty --json | grep "\"name\": \"$NAME\"" -B 1 | head -n 1 | sed -r 's/[^0-9]//g'
    fi
}

#Debug
#getDNSRecordNameExtWildCardA
#getDNSRecordNameACME
#exit 0

if [ "$WILDCARD" == "yes" ]; then
    echo "WILDCARD issue: $WILDCARD"

    if [ "$DO_ACTION" == "add" ]; then
        #addDNSRecord
        echo  "[taskq_dns_post | info]: Operation: $DO_ACTION" | tee -a $REPORT_FILE
        LinodeDomainID=$(getLinodeDomainID $DOMAIN)
        DNSRecordNameACME=$(getDNSRecordNameACME $DOMAIN)
        DNSRecordNameExtWildCardA=$(getDNSRecordNameExtWildCardA $DOMAIN)
        DNSRecordNameExtWildCardAAAA=$(getDNSRecordNameExtWildCardAAAA $DOMAIN)
        # For ACME record
        if [ -z "$DNSRecordNameACME" ]; then # If record empty then add new
            echo "[taskq_dns_post | info]: OK, no existing record name. adding new ACME record ... " | tee -a $REPORT_FILE
            $LPYENV domains records-create $LinodeDomainID --pretty --json \
                --name $NAME \
                --target $VALUE \
                --type $TYPE \
                --ttl_sec $DNS_USER_TTL_SEC
            echo "[taskq_dns_post | info]: OK, DNS record entry $NAME for $DOMAIN has been added with TTL_SEC = \"$DNS_USER_TTL_SEC\"." | tee -a $REPORT_FILE
            #sleep 60m
        else
            WARNING_STATUS="WARNING"
            echo "[taskq_dns_post | info]: Warning, unable to add new record for \"$NAME\" because existing record name \"$DNSRecordNameACME\" is found." | tee -a $REPORT_FILE
        fi

        # For wildcard A record
        if [ -z "$DNSRecordNameExtWildCardA" ]; then # If record empty then add new
            # For Wildcard domain records ( add 2 records *.domain.com domain.com)
            echo "[taskq_dns_post | info]: OK, no existing ipv4 record name. Adding new wildcard domain [$WILDCARD_SELECT0] ... " | tee -a $REPORT_FILE
            $LPYENV domains records-create $LinodeDomainID --pretty --json \
                --name $WILDCARD_SELECT0 \
                --target $ipv4 \
                --type "A" \
                --ttl_sec $DNS_USER_TTL_SEC
            echo "[taskq_dns_post | info]: OK, DNS record entry $WILDCARD_SELECT0 for $DOMAIN has been added with TTL_SEC = \"$DNS_USER_TTL_SEC\"." | tee -a $REPORT_FILE
            #sleep 60m
        else
            WARNING_STATUS="WARNING"
            echo "[taskq_dns_post | info]: Warning, unable to add new wildcard domain (ipv4) for \"$WILDCARD_SELECT0\" because existing record name is found." | tee -a $REPORT_FILE
        fi

        if [ -z "$DNSRecordNameExtWildCardAAAA" ]; then # If record empty then add new
            echo "[taskq_dns_post | info]: OK, no existing ipv6 record name. Adding new wildcard domain [$WILDCARD_SELECT0] ... " | tee -a $REPORT_FILE
            $LPYENV domains records-create $LinodeDomainID --pretty --json \
                --name $WILDCARD_SELECT0 \
                --target $ipv6 \
                --type "AAAA" \
                --ttl_sec $DNS_USER_TTL_SEC
            echo "[taskq_dns_post | info]: OK, DNS record entry $WILDCARD_SELECT0 for $DOMAIN has been added with TTL_SEC = \"$DNS_USER_TTL_SEC\"." | tee -a $REPORT_FILE
            #sleep 60m
        else
            WARNING_STATUS="WARNING"
            echo "[taskq_dns_post | info]: Warning, unable to add new wildcard domain (ipv6) for \"$WILDCARD_SELECT0\" because existing record name is found." | tee -a $REPORT_FILE
        fi

    elif [ "$DO_ACTION" == "delete" ]; then
        #deleteDNSRecord
        echo  "[taskq_dns_post | info]: Operation: $DO_ACTION" | tee -a $REPORT_FILE
        LinodeDomainID=$(getLinodeDomainID $DOMAIN)
        DNSRecordID=$(getDNSRecordID $DOMAIN)

        if [ -n "$DNSRecordID" ]; then # If ID exist (not empty) then can do delete operation
            echo "[taskq_dns_post | info]: OK, DNSRecordID found "
            $LPYENV domains records-delete $LinodeDomainID $DNSRecordID
            echo "[taskq_dns_post | info]: OK, DNS record entry $NAME for $DOMAIN has been deleted." | tee -a $REPORT_FILE
        else
            echo "[taskq_dns_post | info]: Warning, unable to delete existing record _acme-challenge or _acme-challenge-test for $DOMAIN because DNSRecordID returns empty value." | tee -a $REPORT_FILE
        fi

    elif [ "$DO_ACTION" == "update" ]; then
        #updateDNSRecord
        echo  "[taskq_dns_post | info]: Operation: $DO_ACTION" | tee -a $REPORT_FILE
        LinodeDomainID=$(getLinodeDomainID $DOMAIN)
        DNSRecordID=$(getDNSRecordID $DOMAIN)

        if [ -n "$DNSRecordID" ]; then # If record ID exist, then can do update operation
            echo "[taskq_dns_post | info]: OK, DNSRecordID found "
            $LPYENV domains records-update $LinodeDomainID $DNSRecordID --pretty --json \
                --name $NAME \
                --target $VALUE \
                --ttl_sec $DNS_USER_TTL_SEC
            echo "[taskq_dns_post | info]: OK, DNS record for $DOMAIN with entry name \"$NAME\" has been updated to \"$VALUE\" with TTL_SEC = \"$DNS_USER_TTL_SEC\""  | tee -a $REPORT_FILE
        else
            echo "[taskq_dns_post | info]: Warning, unable to update existing record for $DOMAIN because DNSRecordID returns empty value." | tee -a $REPORT_FILE
        fi
    else
        echo "[taskq_dns_post | info]: Warning, unknown action: $DO_ACTION" | tee -a $REPORT_FILE
    fi

fi

echo "=====================================================================" | tee -a $REPORT_FILE

# letsencrypt_post.sh will handle the email send
#$MAIL_BIN  -s "[letsencrypt | $DOMAIN]: Post Script has executed $DO_ACTION operation @ $MYHOSTNAME" $MYEMAIL < $REPORT_FILE
#$MAIL_BIN  -s "[letsencrypt | $DOMAIN]: Post Script has executed $DO_ACTION operation @ $MYHOSTNAME" $MYEMAIL < $REPORT_FILE