#!/bin/bash

# Author - Arafat Ali

#DA external DNS wildcards

LPYENV="/home/admin/maxipy/bin/./linode-cli"

MYEMAIL="webmaster@sofibox.com"
MYHOSTNAME=`/bin/hostname`
WARNING_STATUS="N/A"
MAIL_BIN="/usr/local/bin/mail"
DATE_BIN=`/usr/bin/date +%s`
RM_BIN="/bin/rm"
MV_BIN="/bin/mv"
REPORT_FILE="/tmp/taskq-dns-post-script.log"

DOMAIN=$domain #defined var
DNS_RECORD_NAME="_acme-challenge"
#DNS_RECORD_VALUE="pre-check"
# Get the entry that was saved by letsencrypt.sh
DNS_RECORD_VALUE=$(</usr/local/directadmin/scripts/custom/dns_entry.txt)
DNS_TTL_SEC="300"

#######DEBUGGG#######
#DOMAIN="sofibox.com"
#DNS_RECORD_NAME="_acme-challenge"
#DNS_RECORD_VALUE="XXXX12300909909090909090009090909"
#DNS_TTL_SEC="300"
#################

#RECORD_ID=0

# get Domain ID and print it out (OK)
getLinodeDomainID()
{
        $LPYENV domains list --domain $DOMAIN --pretty --json |
                fgrep '"id":' |
                sed -r 's/[^0-9]//g'
}


# get existing DNS record name and print it out (OK)
getDNSRecordName()
{
        LinodeDomainID=$( getLinodeDomainID $DOMAIN)

        if [ -n "$LinodeDomainID" ] ; then
        #echo "found record name: "
        $LPYENV domains records-list --page 2 $LinodeDomainID --pretty --json |
                grep "\"name\": \"$DNS_RECORD_NAME\"" | grep -o '_acme[^"]challenge*'
        else
                echo "[ WARNING ]: LinodeDomainID returns empty value"
        fi
}

#get existing DNS record ID specific to _acme-challenge (OK)
getDNSRecordID()
{
        LinodeDomainID=$(getLinodeDomainID $DOMAIN)

        if [ -n "$LinodeDomainID" ] ; then
                #echo "found record id: "
                $LPYENV domains records-list --page 2 $LinodeDomainID --pretty --json |
                        grep "\"name\": \"$DNS_RECORD_NAME\"" -B 1 |
                        head -n 1 |
                        sed -r 's/[^0-9]//g'
        else
                echo "[ WARNING ]: LinodeDomainID returns empty value"
        fi
}

# check existing DNS record name if not exist then add else do nothing (OK)
addDNSRecord()
{
        LinodeDomainID=$(getLinodeDomainID $DOMAIN)
        DNSRecordName=$(getDNSRecordName $DOMAIN)

        if [ -z "$DNSRecordName" ] ; then # If record empty then add new
                        echo "exiting record name not found. adding new record ... "
                $LPYENV domains records-create $LinodeDomainID --pretty --json \
                        --name "$DNS_RECORD_NAME" \
                        --target "$DNS_RECORD_VALUE" \
                        --type "TXT" \
                        --ttl_sec "$DNS_TTL_SEC"
        else
                echo "[ WARNING ]: unable to add new record for \"$DNS_RECORD_NAME\" because existing record name \"$DNSRecordName\" is found."
        fi
}


# check existing DNS record if exist then delete else do nothing (OK)
deleteDNSRecord()
{
        LinodeDomainID=$(getLinodeDomainID $DOMAIN)
        DNSRecordID=$(getDNSRecordID $DOMAIN)

        if [ -n "$DNSRecordID" ] ; then # If ID exist (not empty) then can do delete operation
                $LPYENV domains records-delete $LinodeDomainID $DNSRecordID
                echo "DNS record entry $DNS_RECORD_NAME for $DOMAIN has been deleted."
        else
                echo "[ WARNING ]: unable to delete existing record for $DOMAIN because DNSRecordID returns empty value."
        fi
}

# Update DNS rcord
updateDNSRecord()
{
        LinodeDomainID=$(getLinodeDomainID $DOMAIN)
        DNSRecordID=$(getDNSRecordID $DOMAIN)

        if [ -n "$DNSRecordID" ] ; then # If record ID exist, then can do update operation
                $LPYENV domains records-update $LinodeDomainID $DNSRecordID --pretty --json \
                        --name "$DNS_RECORD_NAME" \
                        --target "$DNS_RECORD_VALUE" \
                        --ttl_sec "$DNS_TTL_SEC"
                echo "DNS record for $DOMAIN with entry name \"$DNS_RECORD_NAME\" has been updated to \"$DNS_RECORD_VALUE\""  >> $REPORT_FILE
        else
                echo "[ WARNING ]: unable to update existing record for $DOMAIN because DNSRecordID returns empty value."
                        fi

}


if [[ "${action}" == "request" || "${action}" == "renew" ]]; then
        echo "action: $action"
        updateDNSRecord
else
    echo "not the same"
fi

$MAIL_BIN  -s "[letsencrypt-pre][$WARNING_STATUS] Letsencrypt Pre Script @ $MYHOSTNAME" $MYEMAIL < $REPORT_FILE