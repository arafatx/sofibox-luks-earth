# 1) INITIAL SETUP INSTRUCTION (this guide is messy, once it finishes, it will be in a perfect shape)

Sofibox @ Earth

Operating system: CentOS8
VPS: Linode - Dedicated 32GB - Load Balance East Earth01

### Setup LUKS Encryption DISK on linode

#### 1) Create 2 DISK with the following name
```
/boot - 1024GB - file type must be RAW
/rescue - [the rest of the space] - file type must be RAW
```
#### 2) Create 2 Configuration Profiles with the folling details:
```
Rescue profile
-----------------
Label: Rescue
Kernel: Direct Disk
/dev/sda: Boot disk image.
/dev/sdb: Rescue disk image.
root / boot device: Standard /dev/sdb

Boot profile
------------
Label: Boot
Kernel: Direct Disk
/dev/sda: Boot disk image.
root / boot device: Standard /dev/sda
```

#### 3) Download image file (netboot) CentOS

1) Boot into Rescue Mode with your Installer disk mounted to /dev/sda, and connect to your Linode using the GLish/Lish Console.
2) Download the small iso image from the nearest mirror and extrat the iso image into /dev/sda (The Rescue Partition): 

List of mirror: [Centos Netboot ISO](http://isoredirect.centos.org/centos/8/isos/x86_64/)

```
wget http://centos.ipserverone.com/centos/8.1.1911/isos/x86_64/CentOS-8.1.1911-x86_64-boot.iso
mv CentOS* mini.iso
dd if=mini.iso of=/dev/sda
```


### 4) Install CentOS 8.

1) Reboot into your Rescue configuration profile, and open the Glish graphical console from the Remote Access tab in your Linode’s Dashboard
2) Follow the GUI installer guide how to install centOS, the installer is netboot and it needs internet connection to install. So turn on ethernet connection, and select minimal setup. Don't forget to select Encryption to the partition
3) Select custom partition, and leave everything as default (cent-os will distribute the disk size and make few partitions depends on your disk size)
4) Specifiy encryption paraphrase, and create 2 users (root account and ssh user) durign the installation
5) When you have finished installing, reboot the system then boot the Profile Configuration to enter to the OS. You can delete the Rescue Profile.

### 5) Initial setting up CentOS 8.

1) Everytime when the node is booted, you will have to enter the paraphrase keyto decrypt the partition. 
2) Setup SSH key for SSH user
3) Modify SSH to allow SSH key login only
4) Disable SElinux
5) Change the SSH port (add extra dont delete), allow firewall to the new port eg:

```
firewall-cmd --add-port=3456/tcp --permanent
firewall-cmd --reload
```
6) Reload SSH

```
systemctl reload sshd
```
[Sample SSHD Config File: /etc/ssh/sshd_config](https://gitlab.com/arafatx/sofibox-luks-earth/-/blob/master/etc/ssh/sshd_config)

7) Test the connection, if OK, delete port 22
8) Change timezone for CentOS
9) Change hostname to be FQDN [/etc/hostname](https://gitlab.com/arafatx/sofibox-luks-earth/-/blob/master/etc/hostname)
10) Change hosts IP and include IPv6 [/etc/hosts](https://gitlab.com/arafatx/sofibox-luks-earth/-/blob/master/etc/hosts)
11) Change the interface type from enps* to eth0 and create static IP connection:

##### a) Modify GRUB file
Edit
```
nano /etc/default/grub
```
FROM
```
GRUB_CMDLINE_LINUX="crashkernel=auto resume=/dev/mapper/cl-swap rd.lvm.lv=cl/root rd.lvm.lv=cl/swap rhgb quiet"
```
TO
```
GRUB_CMDLINE_LINUX="crashkernel=auto resume=/dev/mapper/cl-swap rd.lvm.lv=cl/root rd.lvm.lv=cl/swap net.ifnames=0 rhgb quiet"
```

Compile GRUB
```
grub2-mkconfig  -o /boot/grub2/grub.cfg
```

[Sample GRUB file: /etc/default/grub](https://gitlab.com/arafatx/sofibox-luks-earth/-/blob/master/etc/default/grub)

##### b) Change interface from enp0s3 to eth0

mv /etc/sysconfig/network-scripts/ifcfg-enp0s3 /etc/sysconfig/network-scripts/ifcfg-eth0

##### b) edit ifcfg-eth0 to change for static IP:
```
nano /etc/sysconfig/network-scripts/ifcfg-eth0
```
[Sample ifcfg-eth0 file: /etc/sysconfig/network-scripts/ifcfg-eth0](https://gitlab.com/arafatx/sofibox-luks-earth/-/blob/master/etc/sysconfig/network-scripts/ifcfg-eth0)

then reboot

install rsnapshot make sure that you can revert everything if something goes wrong:

```
yum install rsnapshot
```

edit /etc/rsnapshot.conf:

[Sample rsnapshot.conf: /etc/rsnapshot.conf](https://gitlab.com/arafatx/sofibox-luks-earth/-/blob/master/etc/rsnapshot.conf)

add a new rsnapshot script:

[Sample rsnapshot-check.sh: /usr/local/bin/maxicron/rsnapshot/rsnapshot-check.sh](https://gitlab.com/arafatx/sofibox-luks-earth/-/blob/master/usr/local/bin/maxicron/rsnapshot/rsnapshot-check.sh)

create cronjob for rsnapshot, hourly, daily, weekly and monthly

[Sample crontab: /etc/crontab](https://gitlab.com/arafatx/sofibox-luks-earth/-/blob/master/etc/crontab)

### Install Directadmin

Install Directadmin with this guide: [https://www.directadmin.com/installguide.php](https://www.directadmin.com/installguide.php)

a) Preinstall command for CentOS 8
```
yum install wget tar gcc gcc-c++ flex bison make bind bind-libs bind-utils openssl openssl-devel perl quota libaio \
libcom_err-devel libcurl-devel gd zlib-devel zip unzip libcap-devel cronie bzip2 cyrus-sasl-devel perl-ExtUtils-Embed \
autoconf automake libtool which patch mailx bzip2-devel lsof glibc-headers kernel-devel expat-devel \
psmisc net-tools systemd-devel libdb-devel perl-DBI perl-libwww-perl xfsprogs rsyslog logrotate crontabs file \
kernel-headers hostname
```

b) Need to allow SSH users in /etc/ssh/sshd_config 

c) Prepare license information from the DA client:
   USER ID / CLIENT ID: ****
   LICENSE ID: ****
   
d) Install directadmin when finish, install csf, import old blocked ips. 

A nice guide for csf from poralix: https://help.poralix.com/articles/how-to-block-ips-with-csf-directadmin-bfm

then reboot.

[Sample /etc/csf/csf.conf - pending]

then change DA admin password, add authenticion key, security questions. 

Next enable ipv6 ready,

    d-a)  enable ipv6 first before install certificate. make sure to follow this step to enable SSL:
    
    1) recheck: first make sure centos 8 has ipv6 static ip ready [has done this but recheck again] [check sample file here: ] [/etc/sysconfig/network-scripts/ifcfg-eth0](https://gitlab.com/arafatx/sofibox-luks-earth/-/blob/master/etc/sysconfig/network-scripts/ifcfg-eth0)
    
    2) second make sure the /etc/resolv.conf is correct. [check sample file here: ] [/etc/resolve.conf](https://gitlab.com/arafatx/sofibox-luks-earth/-/blob/master/etc/resolve.conf)
    
    3) third make sure /etc/sysctl.conf has ipv6 command enabled [check sample file here: [/etc/sysctl.conf](https://gitlab.com/arafatx/sofibox-luks-earth/-/blob/master/etc/sysctl.conf)]
    
    4) make sure /etc/hosts has ipv6 FQDN here [check sample file here: ] [/etc/hosts](https://gitlab.com/arafatx/sofibox-luks-earth/-/blob/master/etc/hosts)
    
    5) when done doing above, follow this guide to enable ipv6: https://help.directadmin.com/item.php?id=353 
    
        [warning do not enable ssl=1 yet, until certificate is enabled then we can enable ssl=1 or you are not going to be able to renew your cert]
    
    6) make sure that /etc/named.conf is listening to ipv6 other applications are said to be supported ipv6 out of the box 
      
      [check sample file here for /etc/named.conf: ] [/etc/named.conf](https://gitlab.com/arafatx/sofibox-luks-earth/-/blob/master/etc/named.conf)
    
After you have done enabling ipv6, then we can request installing certificate. Follow this step:

    1) If letsencrypt has not installed: Follow this guide to enable letsencrypt: https://help.directadmin.com/item.php?id=648
    
    2) If letsencrypt has enabled, you just need to trigger this command first to create acme challenge dummy text:
        
        ```
        cd /usr/local/directadmin/custombuild
        ./build rewrite_confs
        ```
    3) When done above, you now request ssl certificate by following this guide: https://help.directadmin.com/item.php?id=629 [at this point, you can enable ssl=1 following the guide]
    
    Done for the SSL part
    
e) Then change the evolution skin logo, timeformat, change email address and name and apply to all users -> restart server

f) change phpmyadmin root password, DA root password, roundcube root password, and disable root login update my.cnf and mysql.conf:

    edit this file /var/www/html/phpMyAdmin/config.inc.php
    
    ```
    /* Authentication type */
    $cfg['Servers'][$i]['auth_type'] = 'cookie'; //this allow httpasswd to be used for another layer protection - say who? me.
    $cfg['Servers'][$i]['AllowRoot'] = false; //this disable root login
    ```
g)  use custom configuration at custombuild/custom and change phpmyadmin and roundcube link.. 
   
   Introduction: 
   
   mysql root password for directadmin can be found here: 
   
   ```
   cat /usr/local/directadmin/conf/mysql.conf
   ```
   change the root password, da_roundcube and the da root password inside phpmyadmin. Remove "Any" users for security. Remember to restrict root access from the myphpadmin file.
   
   update my.cnf and mysql.conf username and password:
   
   ```
   nano /usr/local/directadmin/conf/mysql.conf && /usr/local/directadmin/conf/my.cnf
   ```
   
   g-a) For phpmyadmin security:
   
        copy the following files:
        
        #phpMyAdmin (p = preseve attributes, R=recursive)
   cp -p /var/www/html/phpMyAdmin/config.inc.php /usr/local/directadmin/custombuild/custom/phpmyadmin/config.inc.php
   cp -Rp /var/www/html/phpMyAdmin/themes /usr/local/directadmin/custombuild/custom/phpmyadmin/themes
   
   delete the content of themes inside, and put a text file readme at /usr/local/directadmin/custombuild/custom/phpmyadmin/themes/readme.txt with the following content:
  
   
   ```
   theme has not published yet
   ```
   
   create a new .htaccess file at /usr/local/directadmin/custombuild/custom/phpmyadmin/.htaccess with the following content:
   
   ```
   RewriteEngine On
   RewriteCond %{HTTPS} !=on
   RewriteRule ^/?(.*) https://%{SERVER_NAME}/dbchunk$1 [R=301,L]
   ```

   
   
   # [optional] increase memory consumption or upload sql size, make custom path in custom/php.conf.d
   https://forum.directadmin.com/threads/solved-customized-50-webapps-ini-and-10-directadmin-ini.61045/#post-312326
   
   ### TO fix this error in phpMyAdmin
   The phpMyAdmin configuration storage is not completely configured, some extended features have been deactivated. To find out why click here.
  type the following:
cd /root
wget -O create_tables.sh http://files1.directadmin.com/services/all/phpMyAdmin/create_tables.sh
chmod 755 create_tables.sh
./create_tables.sh

   ##
   Now, rebuild phpmyadmin:
   
   ```
   cd /usr/local/directadmin/custombuild
   ./build phpmyadmin
   ```
   
   You won't be able to access the custom path yet that is defined from .htaccess, you need to edit httpd-alias.conf. we will do it later. follow the next step:
   
      [sample file for custom/phpmyadmin: ]
    
   g-b) For roundcube security:
   
   edit this file:
   
   nano var\www\www\html\roundcube\config\config.inc.php
   
   g-b-1) I changed the password that I have modified earlier at this line:
   
   ```
   $config['db_dsnw'] = 'mysql://da_roundcube:NEW_PASSWOWRD_HERE@localhost/da_roundcube';
   ```
   
   g-b-2) I change the product name:
   
   ```
   // Name your service. This is displayed on the login screen and in the window title
    $config['product_name'] = 'MyBox';
   ```
   g-b-3) I enable some built in plugins from roundcube:
   
   ```
   // List of active plugins (in plugins/ directory)
    $config['plugins'] = array(
    'password',
    'archive',
    'zipdownload',
    //'rcguard',
    //'twofactor_gauthenticator',
    'newmail_notifier',
    'managesieve',
    'markasjunk',
    'emoticons',
    'attachment_reminder',
    'new_user_dialog',
    );
   ```
    Let's us https for this roundcube. put this at the end of line config.inc.php
   
   ```
    // ## MaXi32 ## //
    // Force https https://help.directadmin.com/item.php?id=2082
    $config['force_https'] = true; // ## MaXi32 ## //
    // Log errors ?
    // $config['log_logins'] = true;
    ```
   Note that rcguard and twofactor_gauthenticator are 3rd party plugin. we will enable this later and test the current setup first.
   

   Now let's make custom path for roundcube, Copy the following 1 important file:
   
   ```
   mkdir -p /usr/local/directadmin/custombuild/custom/roundcube
   cp -p /var/www/html/roundcube/config/config.inc.php /usr/local/directadmin/custombuild/custom/roundcube/config.inc.php
   cd /usr/local/directadmin/custombuild
   ./build roundcube
   ```
   
   You can test server.sofibox.com/roundcube to see if works. 
   
   
    [sample file for custom/roundcube: ]
    
   g-c) [optional] Create 2 httpasswd users for apache (httpd) to protect roundcube and phpmyadmin link (extra protection). We will use this later:
    
    ```
    sudo htpasswd -c /etc/httpd/.htpasswd roundcubeuser  //with -c
    sudo htpasswd /etc/httpd/.htpasswd phpmyadminuser  //without -c
    sudo chown apache:apache /etc/httpd/.htpasswd
    sudo chmod 0660 /etc/httpd/.htpasswd

    ```
    
   
    g-c-1) copy httpd template and not have custombuild revert it and change apache status to production mode (Prod) then copy the setting to custombuild/custom/ap2 :

    ```
    cd /usr/local/directadmin/custombuild
    mkdir -p custom/ap2
    cp -Rp configure/ap2/conf custom/ap2
    sed -i 's|^ServerTokens .*|ServerTokens Prod|' /etc/httpd/conf/extra/httpd-default.conf
    sed -i 's|^ServerSignature .*|ServerSignature Off|' /etc/httpd/conf/extra/httpd-default.conf
    cp -p /etc/httpd/conf/extra/httpd-default.conf /usr/local/directadmin/custombuild/custom/ap2/conf/extra/
    cp -p /etc/httpd/conf/extra/httpd-mpm.conf /usr/local/directadmin/custombuild/custom/ap2/conf/extra/
    ```
    
    g-c-2) change roundcube and phpmyadmin url and add another protection layer using htpasswd:
    
    Note: if you do this you will get extra protection from bruteforce but, you will lose the feature auto sign in from DA to this 2 apps (roundcube and phpmyadmin)

    ```
    nano /usr/local/directadmin/custombuild/custom/ap2/conf/extra/httpd-alias.conf
    
    # add the following sample:
    ```
    [Sample httpd-alias file: /usr/local/directadmin/custombuild/custom/ap2/conf/extra/httpd-alias.conf](url)
    
    then
    
    ```
    cd /usr/local/directadmin/custombuild
    ./build rewrite_confs
    ```

Now you can test both phpmyadmin and roundcube URL if it works correctly. also test the password.

## Now we change the DA port to the new one [make sure the port is opened in firewall]

Edit this file: /usr/local/directadmin/conf/directadmin.conf , and change port=NEW_PORT

```
port=8181
```
Then edit this file: /var/www/html/redirect.php, and replace the default port 2222 to the new port 

```
<?
header("Location: http://".$_SERVER['HTTP_HOST'].":8181");
?>
```

then run this command,


```
service directadmin restart
```

h) At DA, login as admin (owner), and fix the menu URL of phpmyadmin and roundcube to the one we did above at the skin manager.

Waiting for an answer for this Route URL for phpmyadmin: https://forum.directadmin.com/threads/evolution-skin-how-to-change-the-phpmyadmin-url-route-what-is-route.61090/

then create another admin user -> log in->update the skin logo, timedateformat, and apply to all users, update, both admin URL to the SQL. then disable admin user for security reason


i) create reseller and user package with RESELLER_UNLIMITED AND ADMIN_USER_UNLIMITED. No SSH. to do this, one reseller must be created to manage all domains. do not use admin user as reseller to increase authentication security

ii) Create email for main reseller and setup all skin, user 2step auth, security questions. This reseller is the main user to register normal user. Once done this: Follow the next guide.

iii) Allow CSF to not block local IP temporarily for about 15 days.. because you dont want to get kick out during this setup.

iv) check if IP_SET is supported by Kernel by running this script: https://gitlab.com/arafatx/sofibox-luks-earth/-/blob/master/usr/local/bin/maxiscript/csf_ipset
   
   iv-a) if it supported, it will automatically enable LF_IPSET = "1" in the /etc/csf/csf.conf. You can check your current setting with this command:
   
   ```
    grep -w '^LF_IPSET' /etc/csf/csf.conf
    LF_IPSET = "1"
   ```
   
  iv-b) run this command to run large ipset support in CSF:
   
   ```
   sed -i 's/^DENY_IP_LIMIT = .*/DENY_IP_LIMIT = \"3000\"/' /etc/csf/csf.conf
   sed -i 's/^DENY_TEMP_IP_LIMIT = .*/DENY_TEMP_IP_LIMIT = \"1000\"/' /etc/csf/csf.conf
   csf -ra
   ```
 
 ## Install extension
 Make sure PHP support imap -> https://help.directadmin.com/item.php?id=341
 
 ```
 cd /usr/local/directadmin/custombuild
./build update
./build set_php imap yes
./build php_imap
 ```
 
 Install memcache ->
 
 make sure to allow firewall to open port 11211 (memcached listening port)
 ```
 sudo yum -y install memcached
 sudo systemctl enable --now memcached.service
 sudo systemctl restart memcached
 
 # Check config and status memcached:
  systemctl status memcached
  cat /etc/sysconfig/memcached
 ```
 
 Install memcahche as PHP extension using poralix custom script php-extension.sh
 
 1 ) Enable CentOS PowerTools (important. If not enable cannot install libmemcached-devel)
 
 ```
 dnf config-manager --set-enabled PowerTools
 yum install libmemcached-devel
 cd /usr/local/directadmin/scripts/
 wget https://raw.githubusercontent.com/poralix/directadmin-utils/master/php/php-extension.sh -O php-extension.sh
 chmod 750 php-extension.sh
 ./php-extension.sh
 
 
 # Install php-memcached for all php versions:
 ./php extension.sh install memcached
 
 
 # Install php-geoip for all php versions
 
 yum install geoip
 yum install geoip-devel
 cd /usr/local/directadmin/scripts/
 ./php-extension.sh install geoip --beta
 
 - Currently only beta version work. Without specifying beta, there will be error thrown
 
 # Install log visualiser
 # Make sure to follow above step to install geoip then:

 $ yum install ncurses-devel
 $ cd /tmp
 $ wget https://tar.goaccess.io/goaccess-1.4.tar.gz
 $ tar -xzvf goaccess-1.4.tar.gz
 $ cd goaccess-1.4/
 $ ./configure --enable-utf8 --enable-geoip=legacy
 $ make
 $ make install

 --- NOTE haven't tried goaccess ye for reporting html
 --- for documentation please refer here: https://goaccess.io/download
 ```
 
 
 
 It is time to check DA config to rebuild everything especially directadmin.conf and options.conf
 
 [sample options.conf ]
 [sample directadmin.conf ]
 
 a) create SPF and Dmarc template at /usr/local/directadmin/data/templates/custom/dns_txt.conf with the following contents:
 
 ```
 |DOMAIN|.="v=spf1 a mx ip4:|SERVER_IP||EXTRA_SPF| ~all"
 _dmarc="v=DMARC1; p=none; sp=none; rua=mailto:spam-reports@|DOMAIN|"
 ```
 The variable EXTRA_SPF must be defined in directadmin.conf file
 
 b) create CSF firewall template blacklist at /usr/local/directadmin/data/templates/custom/blacklisted_ip.html:
 
 [Sample blacklisted_ip.html]
 
 c) Create a custom logs.list at /usr/local/directadmin/data/templates/custom/logs.list with the following content:
 
[Sample logs.list file]

 d) Correcting the path for pureftpd by editing the file /etc/rsyslog.conf
 
 ```
 *.info;mail.none;authpriv.none;cron.none;ftp.none       /var/log/messages
 add FTP log
 ftp.*							-/var/log/pureftpd.log
```


 
### Basic Security Directadmin

1) mount tmp partition as nodev,nosuid,noexec and home partition as nosuid
   [Sample fstab file: /etc/fstab](https://gitlab.com/arafatx/sofibox-luks-earth/-/blob/master/etc/fstab)
   
2) disable mysql remote access and add several setting based on mysqltuner 

   then by adding the following line at /etc/my.cnf

    ```
    bind-address = 127.0.0.1
    ```
    install mysql tuner
    
    ```
    wget -O /usr/local/sbin/mysqltuner mysqltuner.pl
    chmod 710 /usr/local/sbin/mysqltuner
    mysqltuner
    ```
    
    when you execute mysqltuner, you will get suggestion what to tweak for your sql. The mysqltuner will read directadmin mysql.conf password that you have specified before.
    
    In case it cannot read and trigger an error. make sure the password doesn't contain this character > or < because mysqltuner does not support it.
    
    [Sample my.cnf at /etc/my.cnf](https://gitlab.com/arafatx/sofibox-luks-earth/-/blob/master/etc/my.cnf)


3) Now it's time to rebuild everything from DA because some exta apps /extension we have listed in the DA custom but we haven't installed that one

```
cd /usr/local/directadmin/custombuild
./build update
./build all d
```

dont forget to also build the php extension

```
./build php_extensions
```


next create reseller account. create email and domain.

add AAA record to server address, edit spf to include ipv6, and add dmarc record.

Now that you can change the skin URL to phpmyadmin and roundcube icon. Disable the route from phpmyadmin.

create reseller email to receive notification from various scripts.

now you can change the server notification page at /var/www/html:

[sample var/www/html: ]




======================

### letsencrypt wild card installation on linode.

In order to make sure that letsencrypt work with wildcard on linode. This is the requirement (I posted this in forum too https://forum.directadmin.com/threads/letsencrypt-issue.59017/post-312937):

To be able to renew the wildcard certificate automatically for external DNS, we need to make sure this:

1) the external DNS server has an API that can be called from DA. I think for open source DNS sever software like BIG-IP DNS has this (eg. linode DNS has their own API).

2) I modified the letsencrypt_pre.sh and I focus only the update API, write a script to update the external DNS from given $DNSENTRY and $domain. The value $domain is supported by letsencrypt_pre.sh. So it can detect which domain can be renew automatically.

3) Create DNS record _acme-challenge = 0 and _acme-challenge-test = 0 for every new domain at the external DNS before we invoke the renewing script.

4) increase wait time at letsencrypt.sh (DIG_SECONDS) (maximum 24 hours) for the domain to propagate properly. we need to make sure that letsencrypt server can ping _acme-challenge or it will fail. Majority problem shown in the forum is this number 4 problem.

5) That's it, the external DNS wildcard can be used anytime, and the renew process is automatic.

====================

a) Since DNSENTRY variable is not supported by letsencrypt_pre.sh (yet), we can save the _acme-challenge value in a text file. So, first edit this file: /usr/local/directadmin/scripts/letsencrypt.sh

Fine line 826:

```
if [ "${CHALLENGETYPE}" = "dns-01" ]; then
                DNSENTRY="`echo -n \"${KEYAUTH}\" | \"${OPENSSL}\" dgst -sha256 -binary | base64_encode`"
```

add new line like this:

```
if [ "${CHALLENGETYPE}" = "dns-01" ]; then
                DNSENTRY="`echo -n \"${KEYAUTH}\" | \"${OPENSSL}\" dgst -sha256 -binary | base64_encode`"
                echo $DNSENTRY > /usr/local/directadmin/scripts/custom/dns_entry.txt
  
```

We will save the DNSENTRY to the text file and from there we will let letsencrypt_pre.sh update our external DNS. Save the file and you are done with this file.


b) now create a file called letsencrypt_pre.sh. The sample file is here: []
======================

### Time to install maxicron automation scripts by Arafat Ali

a) install aide and create aide script - use only local database [sample aide scripts: ]
b) install lynis and create lynis script - use custom path [https://gitlab.com/arafatx/sofibox-luks-earth/-/tree/master/usr/local/bin/maxicron/lynis]
add custom prf "custom.prf at the local path for lynis":

   Get rid of error messages:
   
   a) #! No AIDE database was found, needed for AIDE functionality [FINT-4316]
      disable check AIDE database exist or not. The database is exist at the custom path add skip-test=FINT-4316 -> at custom.prf
   b) # * If not required, consider explicit disabling of core dump in /etc/security/limits.conf file [KRNL-5820]
      disable core dump with the following steps:
      
      1)  Open the file /etc/security/limits.conf to limit '0'.
      
      ```
          # vi /etc/security/limits.conf
      ```    
      
      2) add the following and save the file:
      
      ```
          * hard core 0
      ```
      
      3)  Add this code "fs.suid_dumpable = 0" to file /etc/sysctl.conf
      
      ```
      # echo 'fs.suid_dumpable = 0' >> /etc/sysctl.conf
      # sysctl -p
      ```
      
      4) Now last, add this code "ulimit -S -c 0 > /dev/null 2>&1" to file /etc/profile
      
      ```
      # echo 'ulimit -S -c 0 > /dev/null 2>&1' >> /etc/profile
      ```
      
      [sample lynis custom.prf]

c) Audit daemon is enabled with an empty ruleset. Disable the daemon or define rules [ACCT-9630]

```
#Follow sample from /etc/audit/audit.rules that I have modified
#then restart the service
$ sudo service auditd restart
```

d) Enable process accounting [ACCT-9622]

```
### install psacct
sudo yum install psacct
### then enable it and start it
sudo systemctl enable psacct.service
sudo systemctl start psacct.service
```

then in this d), I can create a bash script for process accounting and send to mail. look at sample: /usr/local/bin/maxicron/psacct/pacc

to show details about lynis test: lynis show details TEST-ID

./lynis show details HOME-9304
2020-05-15 02:22:42 Performing test ID HOME-9304 (Check if users' home directories permissions are 750 or more restrictive)
2020-05-15 02:22:42 Test: checking directory '/home/x' for user 'x'
2020-05-15 02:22:42 Result: permissions of home directory /home/x of user x are fine
2020-05-15 02:22:42 Test: checking directory '/home/admin' for user 'admin'
2020-05-15 02:22:42 Result: permissions of home directory /home/admin of user admin are not strict enough. Should be 750 or more restrictive. Change with: chmod 750 /home/admin
2020-05-15 02:22:42 Test: checking directory '/home/xx' for user 'xx'
2020-05-15 02:22:42 Result: permissions of home directory /home/xx of user xx are not strict enough. Should be 750 or more restrictive. Change with: chmod 750 /home/xx
2020-05-15 02:22:42 Suggestion: Double check the permissions of home directories as some might be not strict enough. [test:HOME-9304] [details:-] [solution:-]
2020-05-15 02:22:42 ====

c) install rclone
to mount use this command:

```
mkdir /root/earthbox
rclone --vfs-cache-mode writes mount onedrive:/EarthBox/ /root/earthbox&
```

If got this error when mount using the method above: 

2020/05/12 22:11:33 Fatal error: failed to mount FUSE fs: fusermount: exec: "fusermount": executable file not found in $PATH

Then you need to install Fuse first

```
yum install fuse
```

#the sign & is to let us mount via background / foreground. this mount method is not permanent, need to create script on startup to active mount and u dont have to put &
below is the step to mount from startup
```

a) create a script to be executed at startup using systemd:

```
nano /etc/systemd/system/onedrive.service

# /etc/systemd/system/onedrive.service
# /etc/systemd/system/onedrive.service
[Unit]
Description=OneDrive (rclone)
AssertPathIsDirectory=/root/earthbox
After=network-online.target

[Service]
Type=simple
User=root
ExecStart=/usr/bin/rclone \
        --config=/root/.config/rclone/rclone.conf \
        --vfs-cache-mode writes mount \
        onedrive:/EarthBox/ /root/earthbox
ExecStop=/bin/fusermount -u /root/earthbox
Restart=always
RestartSec=10

[Install]
WantedBy=default.target


# systemctl daemon-reload
# systemctl enable onedrive.service
# systemctl start onedrive.service
# systemctl reboot
``

d) install rsnapshot
e) chkrootkit

Searching for anomalies in shell history files... Warning: `//root/.python_history
//root/.mysql_history' file size is zero

Just put empty string on both files.

f) install clamav (if follow the custom config file, clamav should be installed by default). But we need to install extra maldet. So:

TIPS: Clamav can ignore file signature. The location to create ignore list is: 

/usr/local/share/clamav/

Example: 

```
Local Signature Ignore (example):
echo "CVE_2012_0773-2" >> /usr/local/share/clamav/whitelist-local.ign2
Third Party Signature Ignore (example):
echo "YARA.r57shell_php_php.UNOFFICIAL" >> /usr/local/share/clamav/whitelist-local.ign2

```
```
cd /usr/local/src
wget -4 http://www.rfxn.com/downloads/maldetect-current.tar.gz
tar -zxvf maldetect-current.tar.gz
cd $(ls -1d maldetect-*/ | tail -1)
./install.sh

```

then modify nano /usr/local/maldetect/conf.maldet 

- change email address alert, enable email alert, change subject: slack_subj="[maldet] Maldet Virus Scan Alert @ $(hostname)"
[sample /usr/local/maldetect/conf.maldet]

- by default maldet create a cron daily script /etc/cron.daily/maldet. No need to change anything

-- now that we need to make sure that the clamav process is not running at startup and other processes should be stop on startup to prevent memory leak.

edit /usr/local/directadmin/data/admin/services.status and turn OFF these processes

clamd=OFF #new
csf=ON
da-popb4smtp=OFF #new
directadmin=ON
dovecot=ON
exim=ON
freshclam=ON
httpd=ON
lfd=ON
mysqld=ON
named=ON
nginx=ON
php-fpm72=ON
php-fpm73=ON
php-fpm74=ON
pure-ftpd=OFF #new
rspamd=ON
sshd=ON

....

create a script to stop the above services (/usr/local/bin/maxiscript/stop-services.sh:

```
#!/bin/bash

# STOP STARTUP LIST #
# ================= #
# stop and disable clamav and freshclam on startup
# useful to prevent clamav re-enable startup when it is updated with the new version
echo "stopping services please wait... "
# Wait for everything to run
sleep 2m
#stop clamav / clamd
sudo systemctl stop clamd
sudo systemctl disable clamav-freshclam.service
sudo systemctl disable clamav-daemon.service
sudo systemctl disable clamav-daemon.socket
sudo systemctl disable clamd
#stop freshclam
sudo systemctl stop freshclam
sudo systemctl disable freshclam
#stop da-popb4smtp
sudo systemctl stop da-popb4smtp
sudo systemctl disable da-popb4smtp
#stop pure-ftpd
sudo systemctl stop pure-ftpd
sudo systemctl disable pure-ftpd
echo "services stopped successfully..."
#===================#
```

then we need to execute the above script on startup. I prefer to use rc.local

```
chmod +x /etc/rc.d/rc.local
nano /etc/rc.d/rc.local
# add this new line
/usr/local/bin/maxiscript/stop-services.sh
```

when reboot this script will run and stop the mentioned services from running after 2 minutes.

also check clamav script and how to enable clamcheck scan in every terminal [https://gitlab.com/arafatx/sofibox-luks-earth/-/tree/master/usr/local/bin/maxicron/clamav]
================

Do this: https://help.directadmin.com/item.php?id=564 (
The phpMyAdmin configuration storage is not completely configured, some extended features have been deactivated.))

- increase roundcube session time out at /usr/local/directadmin/custombuild/custom/roundcube/config.inc.php (add new setting)


```
$config['session_lifetime'] = 10;
```
then

```
cd /usr/local/directadmin/custombuild

./build roundcube
```

rspamd interface?? no need because it will be available in the next release as told by staff. https://forum.directadmin.com/threads/release-rspamd-web-interface-plugin-for-directadmin-v0-1.57856/post-312856



### HOW TO EXTEND THE DISK ? Example making home partition bigger

Since it's raw file type on linode, we can change plan and resize the file system. 

1) I always like to use GUI gparted. So, boot the linode to use gparted live iso:

   b) Boot to rescue disk, the RescueDisk should be the first one (/dev/sda), (dev/sdb)
   
   c) In the Finnix Rescue Mode, download gparted with the option --no-check-certificate 
   
    ```
    wget --no-check-certificate https://jaist.dl.sourceforge.net/project/gparted/gparted-live-stable/1.1.0-1/gparted-live-1.1.0-1-amd64.iso
    mv gparted* gparted.iso
    dd if=gparted.iso of=/dev/sda
    
2) Boot into the Rescue Probile, then follow instruction, then gpart will run. Right click on the encrypted disk, open the LUK encryption, resize the disk using the next unallocated disk. confirm it.
3) Then Open terminal:

    ```
    sudo su -
    
    ```
    after opening the luksEncryption, we can mount to drive like below:

    ```
    #to run chroot command:

    #open luks partition first:
    
    if [ ! -b /dev/mapper/box_earth ]; then
      sudo cryptsetup luksOpen /dev/sda2 box_earth
    fi
    
    
    #lsblk -f #list partition
    
    # mounting each partition
    ##################################
    echo "===================================="
    sudo mount /dev/mapper/box_earth-root /mnt
    sudo mount --bind /dev /mnt/dev
    sudo mount --bind /proc /mnt/proc
    sudo mount --bind /sys /mnt/sys
    
    sudo mkdir /mnt/boot/efi || true
    sudo mount /dev/sda1 /mnt/boot/efi
    
    sudo chroot /mnt
    echo "===================================="
    #################################
    ```
    # or mount using script from the terminal:
    
    ```
    wget https://gitlab.com/arafatx/sofibox-luks-earth/-/raw/master/chroot.sh?inline=false
    # SHORT URL: https://frama.link/chrootx
    chmod +x chroot.sh
    ./chroot.sh
    ```
    
    to exit chroot:
    
    ```
    CTRL + D
    ```
    
    
    
## FIREWALL IPTABLES PROBLEM. Run this in rescue mode

sudo iptables -P INPUT ACCEPT
sudo iptables -P FORWARD ACCEPT
sudo iptables -P OUTPUT ACCEPT
sudo iptables -t nat -F
sudo iptables -t mangle -F
sudo iptables -F
sudo iptables -X

sudo ip6tables -P INPUT ACCEPT
sudo ip6tables -P FORWARD ACCEPT
sudo ip6tables -P OUTPUT ACCEPT
sudo ip6tables -t nat -F
sudo ip6tables -t mangle -F
sudo ip6tables -F
sudo ip6tables -X


### TIPS: to make sure that the nano text editor can use find function [CTRL-W] we need to bind into another key like CTRL-F because CTRL-W will 
### not work in most browser which just terminated the windows. - ArafatX
### to edit that nano key binding, uncomment this file /etc/nanorc: 

bind ^F whereis all (pending sed command for changing this)

install imagemagick by selecting at the build custombuild.. also install imagemagick extension for all php. The settings are there

### HOW TO INSTALL FONT ON CENTOS - useful to use imagemagic

To install font, 

create a folder .font at room home. This folder is hidden. Download fonts and unzip at this folder. Then run the following command to refresh fonts cache in the system:

fc-cache -v
fc-cache-64 -v

# Now we can convert the image
[Sample ascii.txt]:

text 15,15 "                 .88888888:.
                88888888.88888.
              .8888888888888888.
              888888888888888888
              88' _`88'_  `88888
              88 88 88 88  88888
              88_88_::_88_:88888
              88:::,::,:::::8888
              88`:::::::::'`8888
             .88  `::::'    8:88.
            8888            `8:888.
          .8888'             `888888.
          .8888:..  .::.  ...:'8888888:.
        .8888.'     :'     `'::`88:88888
       .8888        '         `.888:8888.
      888:8         .           888:88888
    .888:88        .:           888:88888:
    8888888.       ::           88:888888
    `.::.888.      ::          .88888888
   .::::::.888.    ::         :::`8888'.:.
  ::::::::::.888   '         .::::::::::::
  ::::::::::::.8    '      .:8::::::::::::.
 .::::::::::::::.        .:888:::::::::::::
 :::::::::::::::88:.__..:88888:::::::::::'
  `'.:::::::::::88888888888.88:::::::::'
        `':::_:' -- '' -'-' `':_::::'
        
convert -size 360x360 xc:white -font /root/.fonts/FreeMono.ttf -pointsize 12 -fill black -draw @ascii.txt image.png

Original solutions at: https://unix.stackexchange.com/questions/415246/how-to-install-fonts-for-centos-7

Change email alias: Edit /etc/aliases

find

```
#root: marc
```

change to 

```
youremail.sofibox.com
```

## PENDING Now, since we have done writing the custom URL for both phpmyadmin and roundcube, and everything is good, we can add the missing 2 3rd party plugins (rcguard and 2factor authentication)

rcguard:

https://help.poralix.com/articles/protect-roundcube-with-google-recaptcha-directadmin-server
```
cd /var/www/html/roundcube/plugins/
GIT_SSL_NO_VERIFY=true git clone https://github.com/dsoares/rcguard.git rcguard
chown -R webapps:webapps rcguard/
chmod 755 rcguard
cd rcguard
find . -type d -exec chmod 0755 {} \;
find . -type f -exec chmod 0644 {} \;
mv config.inc.php.dist config.inc.php

nano  config.inc.php
```

// Public key for reCAPTCHA
$rcmail_config['recaptcha_publickey'] = '6LdNmhYTAAAAAOXR**********OcI6MPpePq2eRn';

// Private key for reCAPTCHA
$rcmail_config['recaptcha_privatekey'] = '6LdNmhYTAAAAAB**********vJxvSjDR9VUiDDq-';

```
// Number of failed logins before reCAPTCHA is shown
$rcmail_config['failed_attempts'] = 1;

// Create a table for da_roundcube at phpmyadmin. Run SQL:

CREATE TABLE `rcguard` (
  `ip` VARCHAR(40) NOT NULL,
  `first` DATETIME NOT NULL,
  `last` DATETIME NOT NULL,
  `hits` INT(10) NOT NULL,
  PRIMARY KEY (`ip`),
  INDEX `last_index` (`last`),
  INDEX `hits_index` (`hits`)
) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

// edit roundcube config to include rcguard. edit the custom one:

nano /usr/local/directadmin/custombuild/custom/roundcube/config.inc.php

// find the following line:
// $config['plugins'] = array(

// add this:

// $config['plugins'] = array(
//    'rcguard',

// make a folder for custom roundcube plugins:

mkdir -p /usr/local/directadmin/custombuild/custom/roundcube/plugins/
cd /usr/local/directadmin/custombuild/custom/roundcube/
cp -r /var/www/html/roundcube/plugins/rcguard ./plugins/
chmod 755 plugins
cd plugins
chmod 755 rcguard
cd rcguard
find . -type d -exec chmod 0755 {} \;
find . -type f -exec chmod 0644 {} \;
cd /usr/local/directadmin/custombuild/
./build roundcube

// check permission at /var/www/html/roundcube/plugins/rcguard. done
```

twofactor_gauthenticator: (CANCEL NOT WORKING. BUG) XXXXXXXXXXXXXXXXXXXXXXXXXXXX

```
cd /var/www/html/roundcube/plugins/
GIT_SSL_NO_VERIFY=true git clone https://github.com/alexandregz/twofactor_gauthenticator.git twofactor_gauthenticator
chown -R webapps:webapps twofactor_gauthenticator/
chmod 755 twofactor_gauthenticator
cd twofactor_gauthenticator
find . -type d -exec chmod 0755 {} \;
find . -type f -exec chmod 0644 {} \;
mv config.inc.php.dist config.inc.php
nano config.inc.php

// change this:

// Users allowed to use plugin (IMPORTANT: other users DON'T have plugin activated)
$rcmail_config['users_allowed_2FA'] = array('*@sofibox.com', '*@maxibi.com');

// edit roundcube config to include twofactor_gauthenticator. edit the custom one:

nano /usr/local/directadmin/custombuild/custom/roundcube/config.inc.php

// find the following line:
// $config['plugins'] = array(

// add this:

// $config['plugins'] = array(
//    'twofactor_gauthenticator',

// make a folder for custom roundcube plugins of twofactor_gauthenticator (previous rcguard already create plugins folder):

cd /usr/local/directadmin/custombuild/custom/roundcube/
cp -r /var/www/html/roundcube/plugins/twofactor_gauthenticator ./plugins/
chmod 755 plugins
cd plugins
chmod 755 twofactor_gauthenticator
cd twofactor_gauthenticator
find . -type d -exec chmod 0755 {} \;
find . -type f -exec chmod 0644 {} \;
cd /usr/local/directadmin/custombuild/
./build roundcube

// check permission: perm /var/www/html/roundcube/plugins/twofactor_gauthenticator. done
```

How to restore rsync backup:
1) Create a new volume to hold backup file at linode called backup. Look at the size of the backup (folder size), select that size, example 10GB. Then take for the configuration how to mount and format:

2) Boot into the Rescue Profile (make sure to select this volume and use it on sdc), then follow instruction, then gpart will run. Right click on the encrypted disk, open the LUK encryption:

MOUNT

later..... because need to fine a way to send backup files into the terminal without using shorten url etc...

rsync -aAXv --delete /source /destination

--- later
===============

Install suricata:

1) 
https://redmine.openinfosecfoundation.org/projects/suricata/wiki/RedHat_Enterprise_Linux_8 (NOT WORKING)
https://secfallacy.wordpress.com/2019/11/27/installing-suricata-5-0-0-from-source-on-centos-8 (WORKING) - deprecated, use the latest version 6

```

$ sudo dnf config-manager --set-enabled PowerTools
$ sudo dnf -y install gcc libpcap-devel pcre-devel libyaml-devel file-devel zlib-devel jansson-devel nss-devel libcap-ng-devel libnet-devel tar make libnetfilter_queue-devel lua-devel python3-PyYAML libmaxminddb-devel rustc cargo lz4-devel
$ wget https://www.openinfosecfoundation.org/download/suricata-5.0.3.tar.gz
$ tar xzvf suricata-5.0.3.tar.gz
$ cd suricata-5.0.3/
$ ./configure --libdir=/usr/lib64 --prefix=/usr --sysconfdir=/etc --localstatedir=/var --enable-nfqueue --enable-lua
$ sudo make install-full
$ suricata -V

to uninstall view the path created by suricata-update at github or remove this:

rm -rf /usr/share/suricata
rm -rf /etc/suricata
rm -rf /var/lib/suricata
rm -f /usr/local/bin/suricata

---- NEWWWWWW 11 OCTOBER 2020 ----------------------

# THE BEST TO UPGRADE IS REMOVE ALL PREVIOUS FILES IF I DONT WANT TO MANUALLY UPDATE CONFIG FILE.
UPGRADE SURICATA to v6
wget https://www.openinfosecfoundation.org/download/suricata-6.0.0.tar.gz
tar xzvf suricata-6.0.0.tar.gz
cd suricata-6.0.0/

#./configure --libdir=/usr/lib64 --prefix=/usr/local --sysconfdir=/etc --localstatedir=/var --datarootdir=/usr/local/share --enable-nfqueue --enable-lua
# Dont enable IPS
./configure --libdir=/usr/lib64 --prefix=/usr/local --sysconfdir=/etc --localstatedir=/var --datarootdir=/usr/local/share --enable-lua

sudo make install-full
suricata -V
---- NEWWWWWW 11 OCTOBER 2020 ----------------------
------------------------------------------------

NEW SETTING from RPM:

must add this file:

/etc/sysconfig/suricata

# suricata. A full list can be seen by running /sbin/suricata --help
# -i <network interface device>
# --user <acct name>
# --group <group name>

# Add options to be passed to the daemon
# OPTIONS="-i eth0 --user suricata "
OPTIONS="-i eth0 --user root "


This is Suricata version 5.0.3 RELEASE

There are four log files created by Suricata under the /var/log/suricata directory:

suricata.log: startup messages of Suricata
stats.log: regular statistics about your network traffic
fast.log: suspicious activity found by Suricata
eve.json: the traffic of your local network in JSON messages, and the alerts sent to fast.log in JSON format

```

use suricata update: https://github.com/OISF/suricata-update
and create cronjob: 0 0 * * * *     root    /home/--/maxipy/bin/suricata-update

In /usr/local/bin/suricata-update: put this code:
``
#!/bin/bash

PYTHON_ENV_BIN="/home/--/maxipy/bin/"
SCRIPT="suricata-update"

# Run the update:

"${PYTHON_ENV_BIN}/${SCRIPT}"
``
then,,,

download test file into /opt: https://github.com/OISF/suricata-verify

look at the test file (cd onto it) then test it like this:

suricata -c /etc/suricata/suricata.yaml -r testname.pcap -v

Next create suricata service at systemd: 
```
[Unit]
Description=Suricata IDS/IDP Service
After=network.target
Requires=network.target
Documentation=man:suricata(8) man:suricatasc(8)
Documentation=https://redmine.openinfosecfoundation.org/projects/suricata/wiki

[Service]
Type=forking
Environment=CFG=/etc/suricata/suricata.yaml PID=/var/run/suricata.pid
CapabilityBoundingSet=CAP_NET_ADMIN
PIDFile=/var/run/suricata.pid
ExecStart=/usr/bin/suricata -D -q 0 -q 1 -c $CFG --pidfile $PID -D
ExecReload=/bin/kill -HUP $MAINPID
ExecStop=/bin/kill $MAINPID
PrivateTmp=yes
InaccessibleDirectories=/home /root
ReadOnlyDirectories=/boot /usr /etc

[Install]
WantedBy=multi-user.target
```

```

recommended: https://redmine.openinfosecfoundation.org/issues/2136#change-8331 (WORKING PERFECTLY)

[Unit]
Description=Suricata IDS/IDP Service
Wants=network.target syslog.target
After=network.target syslog.target
Documentation=man:suricata(8) man:suricatasc(8)
Documentation=https://redmine.openinfosecfoundation.org/projects/suricata/wiki

[Service]
Type=forking
ExecStart=/usr/bin/suricata -c /etc/suricata/suricata.yaml --af-packet -vv -D
ExecReload=/bin/kill -HUP $MAINPID
ExecStop=/bin/kill $MAINPID && /bin/rm -f /var/run/suricata.pid
ExecStopPost=/bin/kill $MAINPID && /bin/rm -f /var/run/suricata.pid
Restart=on-failure
RestartSec=15s

[Install]
WantedBy=multi-user.target

```

SystemD startup (from RPM official) -- use this one better

```
# Sample Suricata systemd unit file.
[Unit]
Description=Suricata Intrusion Detection Service
After=syslog.target network-online.target systemd-tmpfiles-setup.service
Documentation=man:suricata(1)

[Service]
# Environment file to pick up $OPTIONS. On Fedora/EL this would be
# /etc/sysconfig/suricata, or on Debian/Ubuntu, /etc/default/suricata.
EnvironmentFile=-/etc/sysconfig/suricata
#EnvironmentFile=-/etc/default/suricata
ExecStartPre=/bin/rm -f /var/run/suricata.pid
ExecStart=/sbin/suricata -c /etc/suricata/suricata.yaml --pidfile /var/run/suricata.pid $OPTIONS
ExecReload=/bin/kill -USR2 $MAINPID

### Security Settings ###
MemoryDenyWriteExecute=true
LockPersonality=true
ProtectControlGroups=true
ProtectKernelModules=true

[Install]
WantedBy=multi-user.target
```


NEW UPDATED suricata.service: (not working)

```

[Unit]
Description=Suricata IDS/IDP Service
After=network.target
Requires=network.target
Documentation=man:suricata(8) man:suricatasc(8)
Documentation=https://redmine.openinfosecfoundation.org/projects/suricata/wiki

[Service]
Type=forking
Environment=CFG=/etc/suricata/suricata.yaml PID=/var/run/suricata.pid
CapabilityBoundingSet=CAP_NET_ADMIN
PIDFile=/var/run/suricata.pid
ExecStart=/usr/bin/suricata -c $CFG --af-packet --pidfile $PID -D
ExecReload=/bin/kill -HUP $MAINPID
ExecStop=/bin/kill $MAINPID
PrivateTmp=yes
InaccessibleDirectories=/home /root
ReadOnlyDirectories=/boot /usr /etc

[Install]
WantedBy=multi-user.target

```
XXXXXXXXXXXXXXX CANCEL BUG XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

Block criteria

```

Look the IP at csf /var/lib/csf/csf.tempban
Look the IP at csf /etc/csf/csf.deny
Check: directadmin lfd /root/blocked_ips.txt
Look at nginx access: 	/var/log/nginx/access_log
Look at apache access: 	/var/log/httpd/access_log

Look at custom csf post: /usr/local/csf/bin/csfpost.sh


```